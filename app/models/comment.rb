class Comment < ActiveRecord::Base
  belongs_to :post
  validates_presence_of :post_id
  #validates :post_id, presence: true
  validates_presence_of :body
  #validates :body, presence: true
end
