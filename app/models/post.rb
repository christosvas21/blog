class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  validates_presence_of :title
  #validates :title, presence: true
  validates_presence_of :body
  #validates :body, presence: true
end
